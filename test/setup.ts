import { configure } from '@testing-library/vue';

// Some slower / overworked machines may need extra time to render deeply nested webcomponents
// This setting gives the await waitFor() checks to be more patient. Be aware that the total
// test duration is also limited, 5 seconds in Vitest by default.
configure({ asyncUtilTimeout: 2500 });

// Vitest normally wouldn't clear the DOM between unit tests, only isolates files.
// @ts-expect-error using the imported form breaks in some environments
globalThis.beforeEach(() => { document.body.innerHTML = ''; });
// Unmount any leftover components in the DOM to allow shutting down scheduled async behaviors
// @ts-expect-error using the imported form breaks in some environments
globalThis.afterEach(() => { document.body.innerHTML = ''; });
