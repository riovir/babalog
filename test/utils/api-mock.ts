import { vi, type Mocked } from 'vitest';
import { type Api } from '#src/api';

export function ApiMock(): Mocked<Api> {
	return {
		ensureChildren: vi.fn().mockRejectedValue('Tried to call addChild() which was not explicitly mocked'),
		addChild: vi.fn().mockRejectedValue('Tried to call addChild() which was not explicitly mocked'),
		getChildren: vi.fn().mockRejectedValue('Tried to call getChildren() which was not explicitly mocked'),
		ensureJournals: vi.fn().mockRejectedValue('Tried to call addChild() which was not explicitly mocked'),
		addJournal: vi.fn().mockRejectedValue('Tried to call addJournal() which was not explicitly mocked'),
		getJournals: vi.fn().mockRejectedValue('Tried to call getJournals() which was not explicitly mocked'),
		removeJournal: vi.fn().mockRejectedValue('Tried to call removeJournal() which was not explicitly mocked'),
		getProfile: vi.fn().mockRejectedValue('Tried to call getProfile() which was not explicitly mocked'),
		getWebId: vi.fn().mockReturnValue('https://testuser.datapod.igrant.io/profile/card#me'),
		handleRedirect: vi.fn().mockResolvedValue(undefined),
		isLoggedIn: vi.fn().mockReturnValue(() => true),
		login: vi.fn().mockResolvedValue(undefined),
		logout: vi.fn().mockResolvedValue(undefined),
	};
}
