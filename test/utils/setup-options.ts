import { vi } from 'vitest';
import { type ComponentMountingOptions } from '@vue/test-utils';
import { type I18n } from 'vue-i18n';
import { type Router } from 'vue-router';
import { createTestingPinia } from '@pinia/testing';
import { API_KEY, i18n as i18nDefault } from '#src/setup';
import { type Api } from '#src/api';
import { ApiMock } from './api-mock';

type Options = {
	api?: Api;
	router?: Router;
	i18n?: I18n;
	provide?: Record<string, unknown>;
};

export function setupOptions({ api, i18n = i18nDefault as NonNullable<Options['i18n']>, router, provide = {} }: Options = {}) {
	const pinia = createTestingPinia({ stubActions: api === undefined, createSpy: vi.fn });
	const plugins = [i18n, pinia, ...maybe(router)];
	provide = { ...provide, [API_KEY as symbol]: api ?? ApiMock() };
	return { global: { renderStubDefaultSlot: true, plugins, provide } } as ComponentMountingOptions<unknown>;
}

function maybe<T>(element?: T) {
	return element ? [element] : [];
}
