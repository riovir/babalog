# Baba-log

> SOLID app to track baby related activities in your own SOLID pod.

## Is this usable?

See for yourself: https://babalog.riovir.com/

Only if you want to log into a specific pod provider, change your name, or want to log that you just fed 45ml of milk to a baby right now. :)
