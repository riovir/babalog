import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

export default defineConfig({
	plugins: [vue()],
	define: {
		'import.meta.env.DATASET_DIR': JSON.stringify(process.env.DATASET_DIR || '/dev/babalog/'),
	},
	publicDir: 'static',
	css: {
		preprocessorOptions: {
			scss: {
				api: 'modern',
				silenceDeprecations: ['mixed-decls'],
			},
		},
	},
});
