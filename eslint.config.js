import jsPlugin from '@eslint/js';
import stylisticPlugin from '@stylistic/eslint-plugin';
import tsPlugin from 'typescript-eslint';
import vuePlugin from 'eslint-plugin-vue';
/** @import { Linter } from 'eslint' */

/** @type {Linter.Config[]} */
export default [
	{
		ignores: [
			'dist/*',
			'node_modules/*',
			'public/*',
		],
	},
	jsPlugin.configs.recommended,
	...tsPlugin.configs.recommended,
	...vuePlugin.configs['flat/recommended'],
	{
		files: ['**/*.{mjs,cjs,js,ts,vue}'],
		rules: {
			'no-alert': 'warn',
			'no-console': ['warn', { allow: ['warn', 'error'] }],
			'no-debugger': 'warn',
			'prefer-const': 'warn',

			'@typescript-eslint/consistent-type-imports': ['warn', { fixStyle: 'inline-type-imports' }],
			'@typescript-eslint/no-explicit-any': 'warn',
			'@typescript-eslint/no-non-null-assertion': 'warn',
			'@typescript-eslint/no-unused-vars': ['warn', { argsIgnorePattern: '^_' }],
			'@typescript-eslint/explicit-module-boundary-types': 'off',
		},
	},
	{
		files: ['**/*.{mjs,cjs,js,ts,vue}'],
		plugins: { '@stylistic': stylisticPlugin },
		rules: {
			'@stylistic/comma-dangle': ['warn', 'always-multiline'],
			'@stylistic/comma-spacing': 'warn',
			'@stylistic/indent': ['warn', 'tab', {
				MemberExpression: 2,
				CallExpression: { arguments: 2 },
				ignoredNodes: ['TemplateLiteral'],
			}],
			'@stylistic/key-spacing': ['warn', { mode: 'strict' }],
			'@stylistic/max-len': ['warn', 140, 2, { ignoreComments: true }],
			'@stylistic/member-delimiter-style': 'warn',
			'@stylistic/no-trailing-spaces': 'warn',
			'@stylistic/object-curly-spacing': ['warn', 'always'],
			'@stylistic/operator-linebreak': ['warn', 'after'],
			'@stylistic/quotes': ['error', 'single', 'avoid-escape'],
			'@stylistic/semi': ['error', 'always'],
			'@stylistic/space-before-function-paren': ['error', { anonymous: 'never', named: 'never', asyncArrow: 'always' }],
			'@stylistic/template-curly-spacing': 'warn',
		},
	},
	{
		files: ['**/*.vue'],
		languageOptions: {
			parserOptions: {
				parser: tsPlugin.parser,
			},
		},
		rules: {
			'vue/component-name-in-template-casing': ['warn', 'PascalCase', { registeredComponentsOnly: false, ignores: [] }],
			'vue/html-closing-bracket-newline': 'warn',
			'vue/html-closing-bracket-spacing': 'warn',
			'vue/max-attributes-per-line': ['warn', { singleline: { max: 3 }, multiline: { max: 1 } }],
			'vue/prop-name-casing': ['warn', 'camelCase'],
			'vue/html-indent': ['warn', 'tab'],
			'vue/singleline-html-element-content-newline': 'off',
		},
	},
];
