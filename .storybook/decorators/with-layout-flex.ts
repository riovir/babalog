import { type Component } from 'vue';

export function withLayoutFlex(Story: () => Component) {
	return {
		template: `
			<div style="display: inline-flex; gap: 12px; align-items: start;">
				<TheStory />
			</div>
		`,
		components: { TheStory: Story() },
	};
}
