import { type Preview, setup } from '@storybook/vue3';
import { i18n, theme } from '#src/setup';
import PrimeVue from 'primevue/config';
import '#src/style/main.scss';
import './storybook.css';

setup(app => app.use(i18n).use(PrimeVue, { theme }));

const preview: Preview = {
	decorators: [
		/** Workaround for Storybook mis-typing date control values on update */
		(Story, { args, argTypes }) => {
			const dateArgs = Object.entries(argTypes)
					.map(([arg, { control }]) => {
						const type = typeof control === 'object' ? control.type : control;
						return type === 'date' ? arg : null;
					})
					.filter(Boolean) as string[];
			dateArgs.forEach(arg => {
				if (typeof args[arg] !== 'number') { return; }
				args[arg] = new Date(args[arg]);
			});
			return Story();
		},
	],
};


export default preview;

document.querySelector('html')?.classList.add('has-theme-dark');
