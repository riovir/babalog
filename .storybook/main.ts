import type { StorybookConfig } from '@storybook/vue3-vite';

const config: StorybookConfig = {
	stories: [
		'../src/**/*.stories.@(js|ts)',
	],
	addons: [
		'@storybook/addon-essentials',
		// '@storybook/addon-a11y',
	],
	framework: '@storybook/vue3-vite',
	docs: {
		autodocs: true,
	},
};
export default config;
