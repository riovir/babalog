import { defineConfig, mergeConfig } from 'vitest/config';
import baseConfig from './vite.config';

const vitestConfig = defineConfig({
	test: {
		coverage: {
			include: ['src/**', 'test/**'],
			exclude: ['**/*.d.ts', '**/*.stories.{js,ts}'],
			reporter: ['text', 'lcov'],
		},
		environment: 'jsdom',
		setupFiles: ['./test/setup.ts'],
		restoreMocks: true,
		// Will allow @testing-library/vue to clean up the DOM after each test
		globals: true,
		server: {
			deps: {
				// Workaround for @topdesk/components-wc transitive dependency issues.
				// Vite will inline these packages, so that Vitest doesn't need to look for them in node_modules.
				// Because if it were, it would get confused by some of the invalid configs, that bundlers tolerate.
				inline: [
					/@lion\/ui/,
					/@open-wc\/scoped-elements/,
					/@topdesk\/components-vue/,
					/@topdesk\/components-wc/,
				],
			},
		},
	},
});

export default mergeConfig(baseConfig, vitestConfig);
