import { type Meta, type StoryObj } from '@storybook/vue3';
import { toRefs } from 'vue';
import { withLayoutFlex } from '#storybook/decorators';
import { BlIcon } from '#src/widgets';
import { phPencil } from '#src/icons';
import BlButton from './bl-button.vue';

export default {
	title: 'Widgets/BlButton',
	args: {
		label: 'Label',
		size: undefined,
	},
	argTypes: {
		size: { control: 'select', options: [undefined, 'small'] },
		onClick: { action: 'click' },
	},
	component: BlButton,
	render: args => ({
		components: { BlButton, BlIcon },
		template: `
			<BlButton v-bind="{ size, label }" @click="onClick" />
			<BlButton v-bind="{ size, label, icon: phPencil }" @click="onClick" />
			<BlButton v-bind="{ size, ariaLabel: label, icon: phPencil }" @click="onClick" />
			<BlButton v-bind="{ size, ariaLabel: label, icon: phPencil }" @click="onClick" rounded />
			<BlButton v-bind="{ size, ariaLabel: label, icon: phPencil }" @click="onClick" rounded text />
			<BlButton v-bind="{ size, label }" @click="onClick" />
		`,
		setup: () => ({ ...toRefs(args), phPencil }),
	}),
	decorators: [withLayoutFlex],
} as Meta<typeof BlButton>;

type Story = StoryObj<typeof BlButton>;

export const Base: Story = {};
export const Small: Story = { args: { size: 'small' } };
