import { type Meta, type StoryObj } from '@storybook/vue3';
import { SbPlaceholder } from '#storybook/components';
import BlEditInline from './bl-edit-inline.vue';

export default {
	title: 'Widgets/BlEditInline',
	component: BlEditInline,
	args: { removable: false },
	argTypes: {
		onApply: { action: 'apply' },
		onCancel: { action: 'cancel' },
		onRemove: { action: 'remove' },
	},
	render: args => ({
		components: { BlEditInline, SbPlaceholder },
		template: `
			<BlEditInline v-bind="{ removable, onApply, onCancel, onRemove }">
				<template #content>
					<SbPlaceholder name="Content" class="has-padding-1x" />
				</template>
				<template #editor>
					<SbPlaceholder name="Editor for the content" interactive autofocus style="width: 100%" />
				</template>
			</BlEditInline>
		`,
		setup: () => args,
	}),
} satisfies Meta<typeof BlEditInline>;

type Story = StoryObj<typeof BlEditInline>;

export const Base: Story = {};
export const Removable: Story = { args: { removable: true } };
