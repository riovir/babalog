import { defineComponent, h } from 'vue';
import { FontAwesomeIcon } from '@riovir/wc-fontawesome';

customElements.define('bl-icon', FontAwesomeIcon);

export default defineComponent({
	setup: props => () => h('bl-icon', props),
});
