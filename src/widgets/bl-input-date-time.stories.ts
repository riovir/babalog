import { type Meta, type StoryObj } from '@storybook/vue3';
import BlInputDateTime from './bl-input-date-time.vue';

export default {
	title: 'Widgets/BlInputDateTime',
	component: BlInputDateTime,
	args: {
		modelValue: new Date(),
	},
	argTypes: {
		modelValue: { control: 'date' },
		'onUpdate:model-value': { action: 'update:model-value' },
	},
	render: args => ({
		components: { BlInputDateTime },
		template: `
			<div style="display: grid; gap: 8px">
				<label id="input-date-time-label">Time</label>
				<small id="input-date-time-help-text">Use HH:MM format</small>
				<BlInputDateTime
					aria-labelledby="input-date-time-label"
					aria-describedby="input-date-time-help-text"
					v-model="modelValue"
					@update:model-value="'onUpdate:model-value'"
				/>
			</div>
		`,
		setup: () => args,
	}),
} satisfies Meta<typeof BlInputDateTime>;

type Story = StoryObj<typeof BlInputDateTime>;

export const Base: Story = {};
