import { type Meta, type StoryObj } from '@storybook/vue3';
import { ref, watch } from 'vue';
import { JournalFeeding } from '#src/domain/journal-feeding';
import LogEntryFeeding from './log-entry-feeding.vue';

export default {
	title: 'components/LogEntryFeeding',
	component: LogEntryFeeding,
	render: args => ({
		template: `
			<LogEntryFeeding v-model="modelValue" @update:model-value="onUpdateModelValue" @remove="onRemove" />
		`,
		components: { LogEntryFeeding },
		setup: () => {
			const modelValue = ref(JournalFeeding(args));
			const syncModelValue = () => { modelValue.value = JournalFeeding(args); };
			watch(() => args.amountMl, syncModelValue);
			watch(() => args.timestamp, syncModelValue);

			return { ...args, modelValue };
		},
	}),
	argTypes: {
		timestamp: { control: 'date' },
		onRemove: { action: 'remove' },
		onUpdateModelValue: { action: 'update:model-value' },
	},
	args: {
		timestamp: new Date(),
		amountMl: 45,
	},
} satisfies Meta;

type Story = StoryObj<typeof LogEntryFeeding>;

export const Base: Story = {};
