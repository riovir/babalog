const BLANK_LIGATURES: string[] = [];
const BLANK_UNICODE_PATH = '';

type SvgPathData = string | string[];

type FaIconArgs = {
	prefix?: string;
	iconName: string;
	viewboxWidth?: number;
	pathData: SvgPathData;
};

type IconData = [
	Required<FaIconArgs['viewboxWidth']>,
	number,
	string[],
	string,
	SvgPathData,
];
export type FaIcon = {
	prefix: Required<FaIconArgs['prefix']>;
	iconName: FaIconArgs['iconName'];
	icon: IconData;
};
export function FaIcon({ prefix = 'ph', iconName, pathData, viewboxWidth = 512 }: FaIconArgs): FaIcon {
	const icon: IconData = [viewboxWidth, 512, BLANK_LIGATURES, BLANK_UNICODE_PATH, pathData];
	return { prefix, iconName, icon };
}
