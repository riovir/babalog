/** Icons from https://icon-sets.iconify.design/ph MIT licensed */
export { default as phPencil } from './ph-pencil';
export { default as phXBold } from './ph-x-bold';
export type { FaIcon } from './fa-icon';
