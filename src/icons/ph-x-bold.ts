import { FaIcon } from './fa-icon';

export default FaIcon({
	iconName: 'x-bold',
	pathData: 'M 416.98 383.02 a 24 24 90 0 1 -34 34 L 256 290 l -127.02 126.98 a 24 24 90 0 1 -34 -34 L 222 256 L 95.02 128.98 a 24 24 90 0 1 34 -34 L 256 222 l 127.02 -127.04 a 24 24 90 0 1 34 34 L 290 256 Z', // eslint-disable-line @stylistic/max-len
});
