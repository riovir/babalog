import { isThing, type Thing } from '@inrupt/solid-client';
import {
	type JournalArgs,
	Journal,
	fromThing as fromGenericThing,
	toThing as toGenericThing,
} from './journal';
import {
	type JournalDiaperArgs,
	JournalDiaper,
	DiaperCategories,
	fromThing as fromDiaperThing,
	toThing as toDiaperThing,
} from './journal-diaper';
import {
	type JournalFeedingArgs,
	JournalFeeding,
	FeedingCategories,
	fromThing as fromFeedingThing,
	toThing as toFeedingThing,
} from './journal-feeding';
export { isJournal, hasUid, childUrlOf } from './journal';

export function createJournal(args?: JournalArgs): Journal;
export function createJournal(args: JournalDiaperArgs): JournalDiaper;
export function createJournal(args: JournalFeedingArgs): JournalFeeding;
export function createJournal(args: JournalArgs | JournalDiaperArgs | JournalFeedingArgs = {}) {
	if ('amountMl' in args || hasCategory(FeedingCategories.FEEDING, args)) {
		return JournalFeeding(args as JournalFeedingArgs);
	}
	if ('color' in args || hasCategory(DiaperCategories.DIAPER, args)) {
		return JournalDiaper(args as JournalDiaperArgs);
	}
	return Journal(args);
}

export function toThing(args: Journal, thing?: Thing): Thing;
export function toThing(args: JournalDiaper, thing?: Thing): Thing;
export function toThing(args: JournalFeeding, thing?: Thing): Thing;
export function toThing(args: Journal | JournalDiaper | JournalFeeding, thing?: Thing) {
	if (hasCategory(DiaperCategories.DIAPER, args)) {
		return toDiaperThing(args as JournalDiaper, thing);
	}
	if (hasCategory(FeedingCategories.FEEDING, args)) {
		return toFeedingThing(args as JournalFeeding, thing);
	}
	return toGenericThing(args as Journal, thing);
}

export function hasCategory(category: string, object: Thing | Pick<JournalArgs, 'categories'>) {
	const { categories = [] } = isThing(object) ? fromGenericThing(object) : object;
	return categories.includes(category);
}


export function fromThing(thing: Thing) {
	if (hasCategory(DiaperCategories.DIAPER, thing)) {
		return fromDiaperThing(thing);
	}
	if (hasCategory(FeedingCategories.FEEDING, thing)) {
		return fromFeedingThing(thing);
	}
	return fromGenericThing(thing);
}
