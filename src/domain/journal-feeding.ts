import { ICAL, RDF, SCHEMA_INRUPT } from '@inrupt/vocab-common-rdf';
import {
	createThing,
	type Thing,
	getStringNoLocale,
	getStringNoLocaleAll,
	addUrl,
	setInteger,
	setStringNoLocale,
	getInteger,
} from '@inrupt/solid-client';
import { pipe } from 'ramda';
import { Journal, type JournalArgs, toThing as toJournalThing, fromThing as fromJournalThing, isJournal } from './journal';

const SCHEMA = Object.freeze({
	/** https://schema.org/QuantitativeValue */
	QuantitativeValue: SCHEMA_INRUPT.NS('QuantitativeValue'),
	/** https://schema.org/unitCode */
	unitCode: SCHEMA_INRUPT.NS('unitCode'),
	/** https://schema.org/value */
	value: SCHEMA_INRUPT.value,
});

export const FeedingCategories = Object.freeze({
	FEEDING: 'FEEDING',
	BY_BABY_BOTTLE: 'BY_BABY_BOTTLE',
	BY_BREAST_LEFT: 'BY_BREAST_LEFT',
	BY_BREAST_RIGHT: 'BY_BREAST_RIGHT',
	MILK_FORMULA: 'MILK_FORMULA',
	MILK_BREST_FRESH: 'MILK_BREST_FRESH',
	MILK_BREST_FROZEN: 'MILK_BREST_FROZEN',
});
export type JournalFeedingCategory = keyof typeof FeedingCategories;

/**
 * UN/CEFACT Common Codes for Units of Measurement
 * See: http://wiki.goodrelations-vocabulary.org/Documentation/UN/CEFACT_Common_Codes
 */
const UnitCodes = Object.freeze({
	MILLILITERS: 'MLT',
	CENTILITERS: 'CLT',
	DECILITERS: 'DLT',
	LITERS: 'LTR',
});
type UnitCode = typeof UnitCodes[keyof typeof UnitCodes];

export type JournalFeedingArgs = Omit<JournalArgs, 'categories'> & {
	categories?: JournalFeedingCategory[];
	amountMl?: number | null;
};

export function JournalFeeding({ categories = [], amountMl = null, ...journalArgs }: JournalFeedingArgs = {}) {
	const journal = Journal({
		...journalArgs,
		categories: [...categories, FeedingCategories.FEEDING],
	});
	return { ...journal, categories: journal.categories as JournalFeedingCategory[], amountMl };
}
export type JournalFeeding = ReturnType<typeof JournalFeeding>;

export function toThing(
	{ amountMl, ...rest }: JournalFeeding,
	thing: Thing = createThing(),
) {
	return pipe(
			(thing: Thing) => toJournalThing(rest, thing),
			(thing: Thing) => amountMl !== null ? addUrl(thing, RDF.type, SCHEMA.QuantitativeValue) : thing,
			(thing: Thing) => amountMl !== null ? setInteger(thing, SCHEMA.value, amountMl) : thing,
			(thing: Thing) => amountMl !== null ? setStringNoLocale(thing, SCHEMA.unitCode, UnitCodes.MILLILITERS) : thing,
	)(thing);
}

export function fromThing(thing: Thing) {
	return JournalFeeding({
		...fromJournalThing(thing),
		categories: filterKnownCategories(getStringNoLocaleAll(thing, ICAL.categories)) ?? undefined,
		amountMl: toMilliliters({
			value: getInteger(thing, SCHEMA.value) ?? 0,
			unitCode: getStringNoLocale(thing, SCHEMA.unitCode) as UnitCode,
		}),
	});
}

function toMilliliters({ value, unitCode }: { value: number; unitCode: UnitCode }) {
	if (unitCode === UnitCodes.MILLILITERS) { return value; }
	if (unitCode === UnitCodes.CENTILITERS) { return value * 10; }
	if (unitCode === UnitCodes.DECILITERS) { return value * 100; }
	if (unitCode === UnitCodes.LITERS) { return value * 1000; }
	console.warn('Unrecognized QualifiedValue', { value, unitCode });
	return value;
}

function filterKnownCategories(values: string[]): JournalFeedingCategory[] {
	const categories = Object.values(FeedingCategories);
	return categories.filter(category => values.includes(category));
}

export function isJournalFeeding(journal: Journal): journal is JournalFeeding {
	return 0 < filterKnownCategories(journal.categories).length;
}

export function isJournalFeedingThing(thing: Thing) {
	if (!isJournal(thing)) { return false; }
	const { categories } = fromJournalThing(thing);
	return 0 < filterKnownCategories(categories).length;
}
