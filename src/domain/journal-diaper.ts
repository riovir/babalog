import { ICAL, SCHEMA_INRUPT } from '@inrupt/vocab-common-rdf';
import {
	createThing,
	type Thing,
	getStringNoLocale,
	getStringNoLocaleAll,
	setStringNoLocale,
} from '@inrupt/solid-client';
import { pipe } from 'ramda';
import { Journal, type JournalArgs, toThing as toJournalThing, fromThing as fromJournalThing, isJournal } from './journal';

const SCHEMA = Object.freeze({
	/** https://schema.org/color */
	color: SCHEMA_INRUPT.NS('color'),
});

export const DiaperCategories = Object.freeze({
	DIAPER: 'DIAPER',
	PEE: 'PEE',
	POO: 'POO',
});
export type JournalDiaperCategory = keyof typeof DiaperCategories;

export type JournalDiaperArgs = Omit<JournalArgs, 'categories'> & {
	categories?: JournalDiaperCategory[];
	color?: string | null;
};

export function JournalDiaper({ categories = [], color = null, ...journalArgs }: JournalDiaperArgs = {}) {
	const journal = Journal({
		...journalArgs,
		categories: [...categories, DiaperCategories.DIAPER],
	});
	return { ...journal, categories: journal.categories as JournalDiaperCategory[], color };
}
export type JournalDiaper = ReturnType<typeof JournalDiaper>;

export function toThing(
	{ color, ...rest }: JournalDiaper,
	thing: Thing = createThing(),
) {
	return pipe(
			(thing: Thing) => toJournalThing(rest, thing),
			(thing: Thing) => color !== null ? setStringNoLocale(thing, SCHEMA.color, color) : thing,
	)(thing);
}

export function fromThing(thing: Thing) {
	return JournalDiaper({
		...fromJournalThing(thing),
		categories: filterKnownCategories(getStringNoLocaleAll(thing, ICAL.categories)) ?? undefined,
		color: getStringNoLocale(thing, SCHEMA.color) ?? null,
	});
}

function filterKnownCategories(values: string[]): JournalDiaperCategory[] {
	const categories = Object.values(DiaperCategories);
	return categories.filter(category => values.includes(category));
}

export function isJournalDiaper(journal: Journal): journal is JournalDiaper {
	return 0 < filterKnownCategories(journal.categories).length;
}

export function isJournalDiaperThing(thing: Thing) {
	if (!isJournal(thing)) { return false; }
	const { categories } = fromJournalThing(thing);
	return 0 < filterKnownCategories(categories).length;
}
