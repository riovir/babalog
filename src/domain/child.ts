import { FOAF, RDF, VCARD } from '@inrupt/vocab-common-rdf';
import {
	setStringNoLocale,
	createThing,
	type Thing,
	getStringNoLocale,
	setDate,
	getDate,
	getUrlAll,
	addUrl,
} from '@inrupt/solid-client';
import { pipe } from 'ramda';

export type ChildArgs = {
	uid?: string;
	url?: string | null;
	birthday?: Date | null;
	nickname?: string | null;
};

export function Child({
	uid = crypto.randomUUID(),
	url = null,
	birthday = null,
	nickname = '',
}: ChildArgs = {}) {
	return { uid, url, birthday, nickname };
}
export type Child = ReturnType<typeof Child>;

/** See docs: https://www.w3.org/TR/vcard-rdf */
export function toThing({ uid, birthday, nickname }: Child) {
	return pipe(
			(thing: Thing) => addUrl(thing, RDF.type, VCARD.Child),
			(thing: Thing) => setStringNoLocale(thing, VCARD.hasUID, uid),
			(thing: Thing) => nickname ? setStringNoLocale(thing, VCARD.nickname, nickname): thing,
			(thing: Thing) => birthday ? setDate(thing, VCARD.bday, birthday): thing,

			(thing: Thing) => addUrl(thing, RDF.type, FOAF.Person),
			/* There is no UID support in FOAF */
			(thing: Thing) => nickname ? setStringNoLocale(thing, FOAF.nick, nickname): thing,
			(thing: Thing) => birthday ? setDate(thing, FOAF.birthday, birthday): thing,
	)(createThing());
}
export function fromThing(thing: Thing) {
	return Child({
		url: thing.url,
		uid: getStringNoLocale(thing, VCARD.hasUID) ?? undefined,
		nickname: (getStringNoLocale(thing, VCARD.nickname) || getStringNoLocale(thing, FOAF.nick)) ?? undefined,
		birthday: (getDate(thing, VCARD.bday) || getDate(thing, FOAF.birthday)) ?? undefined,
	});
}

export function isChild(thing: Thing) {
	return getUrlAll(thing, RDF.type).find(url => [VCARD.Child, FOAF.Person].includes(url));
}

export function hasUid(value: string) {
	return (thing: Thing) => getStringNoLocale(thing, VCARD.hasUID) === value;
}
