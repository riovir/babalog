import { ICAL, RDF } from '@inrupt/vocab-common-rdf';
import {
	createThing,
	type Thing,
	getStringNoLocale,
	getDatetime,
	getUrl,
	getUrlAll,
	getStringNoLocaleAll,
	addStringNoLocale,
	addUrl,
	setDatetime,
	setStringNoLocale,
	setUrl,
	isThing,
} from '@inrupt/solid-client';
import { pipe, uniq } from 'ramda';

export type JournalArgs = {
	uid?: string;
	url?: string | null;
	attendees?: string[];
	categories?: string[];
	timestamp?: Date;
	comments?: string[];
	description?: string;
	organizer?: string | null;
	summary?: string;
};

export function Journal({
	uid = crypto.randomUUID(),
	url = null,
	attendees = [],
	categories = [],
	timestamp = new Date(),
	comments = [],
	description = '',
	organizer = null,
	summary = '',
}: JournalArgs = {}) {
	const attendeesUniq = uniq(attendees).sort();
	const categoriesUniq = uniq(categories).sort();
	return {
		uid, url, timestamp, comments, description, organizer, summary,
		attendees: attendeesUniq, categories: categoriesUniq,
	};
}
export type Journal = ReturnType<typeof Journal>;

/** See docs: https://www.kanzaki.com/docs/ical */
export function toThing(
	{ uid, attendees, categories, timestamp, comments, description, organizer, summary }: Journal,
	thing: Thing = createThing(),
) {
	return pipe(
			(thing: Thing) => addUrl(thing, RDF.type, ICAL.Vjournal),
			(thing: Thing) => setStringNoLocale(thing, ICAL.uid, uid),
			(thing: Thing) => attendees.reduce((acc, attendee) => addUrl(acc, ICAL.attendee, attendee), thing),
			(thing: Thing) => categories.reduce((acc, category) => addStringNoLocale(acc, ICAL.categories, category), thing),
			(thing: Thing) => comments.reduce((acc, comment) => addStringNoLocale(acc, ICAL.comment, comment), thing),
			(thing: Thing) => description ? setStringNoLocale(thing, ICAL.description, description) : thing,
			(thing: Thing) => organizer ? setUrl(thing, ICAL.organizer, organizer) : thing,
			(thing: Thing) => summary ? setStringNoLocale(thing, ICAL.summary, summary) : thing,
			(thing: Thing) => setDatetime(thing, ICAL.dtstamp, timestamp),
	)(thing);
}

export function fromThing(thing: Thing) {
	if (!isJournal(thing)) {
		throw Object.assign(new Error('Thing not recognized as Journal'), { thing });
	}
	return Journal({
		url: thing.url,
		uid: getStringNoLocale(thing, ICAL.uid) ?? undefined,
		attendees: getUrlAll(thing, ICAL.attendee) ?? undefined,
		categories: getStringNoLocaleAll(thing, ICAL.categories) ?? undefined,
		comments: getStringNoLocaleAll(thing, ICAL.comment) ?? undefined,
		description: getStringNoLocale(thing, ICAL.description) ?? undefined,
		organizer: getUrl(thing, ICAL.organizer) ?? undefined,
		summary: getStringNoLocale(thing, ICAL.summary) ?? undefined,
		timestamp: getDatetime(thing, ICAL.dtstamp) ?? undefined,
	});
}

export function isJournal<T>(thing: T | Thing): thing is Thing {
	if (!isThing(thing)) { return false; };
	const types = getUrlAll(thing, RDF.type);
	return types.includes(ICAL.Vjournal);
}

export function hasUid(value: string) {
	return (thing: Thing) => getStringNoLocale(thing, ICAL.uid) === value;
}

export function childUrlOf({ organizer, attendees }: Journal) {
	return attendees.find(attendee => attendee !== organizer);
}
