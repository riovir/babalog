import { RDF, SCHEMA_INRUPT } from '@inrupt/vocab-common-rdf';
import {
	setStringNoLocale,
	setUrl,
	setInteger,
	createThing,
	type Thing,
	getStringNoLocale,
	getUrl,
	getInteger,
} from '@inrupt/solid-client';
import { pipe } from 'ramda';

const SCHEMA = Object.freeze({
	/** https://schema.org/QuantitativeValue */
	QuantitativeValue: SCHEMA_INRUPT.NS('QuantitativeValue'),
	/** https://schema.org/unitCode */
	unitCode: SCHEMA_INRUPT.NS('unitCode'),
	/** https://schema.org/value */
	value: SCHEMA_INRUPT.value,
});

/**
 * UN/CEFACT Common Codes for Units of Measurement
 * See: http://wiki.goodrelations-vocabulary.org/Documentation/UN/CEFACT_Common_Codes
 * */
type UnitCode =
	'MLT' /* milliliter */ |
	'CLT' /* centiliter */ |
	'DLT' /* deciliter */ |
	'LTR' /* liter */ |
	string;

export type QuantitativeValueArgs = {
	url?: string | null;
	value?: number | null;
	unitCode?: UnitCode;
};

export function QuantitativeValue({
	url = null,
	value = null,
	unitCode = 'MLT',
}: QuantitativeValueArgs = {}) {
	return { url, value, unitCode };
}
export type QuantitativeValue = ReturnType<typeof QuantitativeValue>;

export function toThing({ value, unitCode }: QuantitativeValue) {
	return pipe(
			(thing: Thing) => setUrl(thing, RDF.type, SCHEMA.QuantitativeValue),
			(thing: Thing) => value ? setInteger(thing, SCHEMA.value, value) : thing,
			(thing: Thing) => unitCode ? setStringNoLocale(thing, SCHEMA.unitCode, unitCode) : thing,
	)(createThing());
}

export function fromThing(thing: Thing) {
	return QuantitativeValue({
		url: thing.url,
		value: getInteger(thing, SCHEMA.value) ?? undefined,
		unitCode: getStringNoLocale(thing, SCHEMA.unitCode) ?? undefined,
	});
}

export function isQuantitativeValue(thing: Thing) {
	return getUrl(thing, RDF.type) === SCHEMA.QuantitativeValue;
}
