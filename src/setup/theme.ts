import { definePreset } from '@primevue/themes';
import presetBase from '@primevue/themes/lara';

const preset = definePreset(presetBase, {
	semantic: {
		primary: ColorSet('lime'),
	},
});

export const theme = {
	preset,
	options: {
		darkModeSelector: '.has-theme-dark',
	},
};

function ColorSet(name: string) {
	return {
		50: `{${name}.50}`,
		100: `{${name}.100}`,
		200: `{${name}.200}`,
		300: `{${name}.300}`,
		400: `{${name}.400}`,
		500: `{${name}.500}`,
		600: `{${name}.600}`,
		700: `{${name}.700}`,
		800: `{${name}.800}`,
		900: `{${name}.900}`,
		950: `{${name}.950}`,
	};
}
