import { createI18n } from 'vue-i18n';

import hu from '../resources/hu-HU';
import en from '../resources/en-US';

export const i18n = createI18n({
	legacy: false,
	locale: 'hu',
	fallbackLocale: 'en',
	messages: { hu, en },
});
