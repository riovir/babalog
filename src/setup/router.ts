import { createRouter, createWebHistory, type RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
	{ path: '/', redirect: 'hu-HU' },
	{ name: 'home', path: '/:locale', props: true, component: () => import(/* webpackChunkName: "the-app" */ '../the-app.vue') },
];

export const router = createRouter({
	routes,
	history: createWebHistory(import.meta.env.BASE_URL),
});
