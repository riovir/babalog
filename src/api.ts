import {
	getSolidDataset,
	getThing,
	getUrlAll,
	saveSolidDatasetAt,
	createSolidDataset,
	getThingAll,
	type SolidDataset,
	setThing,
	getSourceUrl,
	getStringNoLocale,
	createThing,
	removeThing,
} from '@inrupt/solid-client';
import { handleIncomingRedirect, type Session } from '@inrupt/solid-client-authn-browser';
import { FOAF } from '@inrupt/vocab-common-rdf';
import {
	createJournal,
	isJournal,
	fromThing as journalFromThing,
	toThing as journalToThing,
	hasUid as journalHasUid,
} from '#src/domain/journals';
import { type Journal as JournalGeneric } from '#src/domain/journal';
import {
	Child, isChild,
	fromThing as childFromThing,
	toThing as childToThing,
	hasUid as childHasUid,
} from './domain/child';

export type ApiResponse<T> = Promise<{ dataset: SolidDataset; value: T }>;

export function Api({ session }: { session: Session }) {
	const isLoggedIn = () => !!session.info?.isLoggedIn;
	const login = session.login.bind(session);
	const logout = session.logout.bind(session);
	const handleRedirect = () => handleIncomingRedirect({ restorePreviousSession: true });
	const getWebId = () => session.info?.webId ?? null;

	const getProfile = async () => {
		if (!isLoggedIn()) { throw NotLoggedInError(); }

		const webID = getWebId() ?? never();
		const docUrl = withoutHash(webID);
		const dataset = await getSolidDataset(docUrl, session);
		const profile = getThing(dataset, webID) ?? never();
		const name = getStringNoLocale(profile, FOAF.name);
		return { dataset, value: { name } };
	};

	async function ensureChildren({ dir, datasetUrl }: { dir?: string; datasetUrl?: string }) {
		if (!dir && !datasetUrl) { throw new Error('At least dir or datasetUrl has to be defined'); }
		return datasetUrl ?
			ensureContainerNew(datasetUrl, session) :
			ensureDatasetNew({ session, dir: dir ?? never(), document: 'children.ttl' });
	}

	async function getChildren({ dir }: { dir: string }): ApiResponse<Child[]>;
	async function getChildren({ dataset }: { dataset: SolidDataset }): ApiResponse<Child[]>;
	async function getChildren({ dataset, dir = '' }: { dataset?: SolidDataset; dir?: string }): ApiResponse<Child[]> {
		const datasetResolved = dataset ?? await ensureDataset({ session, dir, document: 'children.ttl' });
		const value = getThingAll(datasetResolved)
				.filter(isChild)
				.map(childFromThing);
		return { dataset: datasetResolved, value };
	};

	async function addChild({ dir, child }: { dir: string; child: Child }): ApiResponse<Child>;
	async function addChild({ dataset, child }: { dataset: SolidDataset; child: Child }): ApiResponse<Child>;
	async function addChild({ dataset, dir = '', child }: { dataset?: SolidDataset; dir?: string; child: Child }) {
		const datasetResolved = dataset ?? await ensureDataset({ session, dir, document: 'children.ttl' });
		const thing = childToThing(Child(child));
		const updatedList = setThing(datasetResolved, thing);
		const url = getSourceUrl(datasetResolved) ?? never();
		const updatedDataset = await saveSolidDatasetAt(url, updatedList, session);
		const valueAdded = childFromThing(getThingAll(updatedDataset).find(childHasUid(child.uid)) ?? never());
		return { dataset: updatedDataset, value: valueAdded };
	}

	async function ensureJournals({ dir, datasetUrl }: { dir?: string; datasetUrl?: string }) {
		if (!dir && !datasetUrl) { throw new Error('At least dir or datasetUrl has to be defined'); }
		return datasetUrl ?
			ensureContainerNew(datasetUrl, session) :
			ensureDatasetNew({ session, dir: dir ?? never(), document: 'journals.ttl' });
	}

	async function getJournals({ dir }: { dir: string }): ApiResponse<JournalGeneric[]>;
	async function getJournals({ dataset }: { dataset: SolidDataset }): ApiResponse<JournalGeneric[]>;
	async function getJournals({ dataset, dir = '' }: { dir?: string; dataset?: SolidDataset }): ApiResponse<JournalGeneric[]> {
		const datasetResolved = dataset ?? await ensureDataset({ session, dir, document: 'journals.ttl' });
		const value = getThingAll(datasetResolved)
				.filter(isJournal)
				.map(journalFromThing);
		return { dataset: datasetResolved, value };
	}

	async function addJournal({ dir, value }: { dir: string; value: JournalGeneric }): ApiResponse<JournalGeneric>;
	async function addJournal({ dataset, value }: { dataset: SolidDataset; value: JournalGeneric }): ApiResponse<JournalGeneric>;
	async function addJournal({ dataset, dir = '', value }: { dataset?: SolidDataset; dir?: string; value: JournalGeneric }) {
		const datasetResolved = dataset ?? await ensureDataset({ session, dir, document: 'journals.ttl' });
		const originalThing = getThingAll(datasetResolved)
				.filter(isJournal)
				.find(journalHasUid(value.uid));
		const thing = journalToThing(createJournal(value), originalThing ?? createThing());
		const updatedList = setThing(datasetResolved, thing);
		const url = getSourceUrl(datasetResolved) ?? never();
		const updatedDataset = await saveSolidDatasetAt(url, updatedList, session);
		const valueAdded = journalFromThing(getThingAll(updatedDataset).find(journalHasUid(value.uid)) ?? never());
		return { dataset: updatedDataset, value: valueAdded };
	}

	async function removeJournal({ dir, value }: { dir: string; value: JournalGeneric }): ApiResponse<boolean>;
	async function removeJournal({ dataset, value }: { dataset: SolidDataset; value: JournalGeneric }): ApiResponse<boolean>;
	async function removeJournal({ dataset, dir = '', value }: { dataset?: SolidDataset; dir?: string; value: JournalGeneric }) {
		const datasetResolved = dataset ?? await ensureDataset({ session, dir, document: 'journals.ttl' });
		const thing = getThingAll(datasetResolved)
				.filter(isJournal)
				.find(journalHasUid(value.uid));
		if (!thing) { return { dataset, value: false }; }

		const updatedList = removeThing(datasetResolved, thing);
		const url = getSourceUrl(datasetResolved) ?? never();
		const updatedDataset = await saveSolidDatasetAt(url, updatedList, session);
		return { dataset: updatedDataset, value: true };
	}


	return {
		isLoggedIn, getWebId, login, logout, handleRedirect,
		getProfile,
		ensureChildren, addChild, getChildren,
		ensureJournals, addJournal, getJournals, removeJournal,
	};
}
export type Api = ReturnType<typeof Api>;

function NotLoggedInError() {
	return new ApiError('Not logged in', { code: 'not_logged_in' });
}

class ApiError extends Error {
	public readonly code: string;

	constructor(message: string, { cause, code }: { cause?: Error; code: string }) {
		super(message, { cause });
		this.name = this.constructor.name;
		this.code = code;
	}
}

async function ensureDataset({ session, dir, document = 'index.ttl' }: { session: Session; dir: string; document: string }) {
	if (!session.info?.isLoggedIn) { throw NotLoggedInError(); }
	const webID = session?.info.webId ?? never();
	const docUrl = withoutHash(webID);
	const dataset = await getSolidDataset(docUrl, session);
	const profile = getThing(dataset, webID) ?? never();
	const [storageUrl] = getUrlAll(profile, 'http://www.w3.org/ns/pim/space#storage');
	const host = /^https?:\/\//i.test(dir) ? '' : storageUrl;
	const path = `${dir}/${document}`.split('/').filter(Boolean).join('/');
	return ensureContainer(`${host}${path}`, session);
}

async function ensureDatasetNew({ session, dir, document = 'index.ttl' }: { session: Session; dir: string; document: string }) {
	if (!session.info?.isLoggedIn) { throw NotLoggedInError(); }
	const webID = session?.info.webId ?? never();
	const docUrl = withoutHash(webID);
	const dataset = await getSolidDataset(docUrl, session);
	const profile = getThing(dataset, webID) ?? never();
	const [storageUrl] = getUrlAll(profile, 'http://www.w3.org/ns/pim/space#storage');
	const host = /^https?:\/\//i.test(dir) ? '' : storageUrl;
	const path = `${dir}/${document}`.split('/').filter(Boolean).join('/');
	return ensureContainerNew(`${host}${path}`, session);
}

async function ensureContainer(url: string, { fetch }: Session) {
	try {
		return await getSolidDataset(url, { fetch });
	} catch (error) {
		if (hasStatusCode(error, 404)) {
			return saveSolidDatasetAt(url, createSolidDataset(), { fetch });
		}
		throw error;
	}
}

async function ensureContainerNew(url: string, { fetch }: Session) {
	try {
		return { url, dataset: await getSolidDataset(url, { fetch }) };
	} catch (error) {
		if (hasStatusCode(error, 404)) {
			return { url, dataset: await saveSolidDatasetAt(url, createSolidDataset(), { fetch }) };
		}
		throw error;
	}
}


function hasStatusCode(error: unknown, code: number): error is Error & { statusCode: number } {
	return (error as { statusCode?: number }).statusCode === code;
}

function withoutHash(urlString: string) {
	const url = new URL(urlString);
	url.hash = '';
	return url.href;
}

function never(): never {
	throw new Error('Should have not happened.');
}
