import { computed, type Ref, ref } from 'vue';

export function useStorage(key: string, fallback: string): Ref<string> {
	try {
		return computed({
			get: () => localStorage.getItem(key) ?? fallback,
			set: (value: string) => {
				localStorage.setItem(key, String(value));
			},
		});
	}
	catch (err) {
		console.warn(`Unable to use localStorage.getItem(${key}). Returning fallback: ${fallback}`, err);
		return ref(fallback);
	}
}
