import './style/main.scss';

import { createApp } from 'vue';
import { RouterView } from 'vue-router';
import PrimeVue from 'primevue/config';
import { api, i18n, router, pinia, theme } from './setup';
import { getDefaultSession } from '@inrupt/solid-client-authn-browser';

createApp(RouterView)
		.use(i18n).use(router).use(pinia)
		.use(PrimeVue, { theme })
		.use(api, { session: getDefaultSession() })
		.mount(document.body);
