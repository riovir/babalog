import { computed, ref } from 'vue';
import { childUrlOf, Journal } from '#src/domain/journal';
import { FeedingCategories, JournalFeeding } from '#src/domain/journal-feeding';
import { type Api, useApi } from '#src/setup';
import { prop, sortBy } from 'ramda';
import { Child } from '#src/domain/child';
import { createJournal } from '#src/domain/journals';
import { DiaperCategories, JournalDiaper } from '#src/domain/journal-diaper';
import { useStorage } from '#src/composables/index';

const sortByTimestamp = sortBy(prop('timestamp'));

export function Store({ api = useApi() } : { api?: Api } = {}) {
	const isLoggedIn = ref(api.isLoggedIn());
	const identityProvider = ref('https://datapod.igrant.io/');
	const userName = ref<null | string>(null);
	const datasetDir = useStorage('dataset-dir', import.meta.env.DATASET_DIR);

	const childrenDatasetUrl = ref<string>();
	const children = ref<Child[]>([]);
	const child = computed(() => children.value[0]);
	const childUrl = computed(() => child.value?.url);

	const journalsDatasetUrl = ref<string>();
	const journalsAll = ref<Journal[]>([]);
	const journals = computed(() => sortByTimestamp(journalsAll.value).toReversed());

	/* Auth */

	const syncIsLoggedIn = () => { isLoggedIn.value = api.isLoggedIn(); };
	const handleRedirect = () => api
			.handleRedirect()
			.then(syncIsLoggedIn)
			.catch(rethrowAfter(syncIsLoggedIn));

	const connectDatasets = async () => {
		const [children, journals] = await Promise.all([
			api.ensureChildren({ dir: datasetDir.value, datasetUrl: childrenDatasetUrl.value }),
			api.ensureJournals({ dir: datasetDir.value, datasetUrl: journalsDatasetUrl.value }),
		]);
		childrenDatasetUrl.value = children.url;
		journalsDatasetUrl.value = journals.url;
	};

	const login = async ({ redirectUrl }: { redirectUrl: string }) => api
			.login({ clientName: 'Babalog', oidcIssuer: identityProvider.value, redirectUrl })
			.then(syncIsLoggedIn)
			.catch(rethrowAfter(syncIsLoggedIn));

	const logout = () => api
			.logout()
			.then(syncIsLoggedIn)
			.then(() => {
				userName.value = null;
				journalsAll.value = [];
			})
			.catch(rethrowAfter(syncIsLoggedIn));

	/* Profile */
	const loadProfile = async () => {
		const { value } = await api.getProfile();
		userName.value = value.name;
	};

	/* Children */
	const childOf = computed(() => (journal: Journal) => {
		const url = childUrlOf(journal);
		return children.value.find(child => child.url === url);
	});

	const addChild = async ({ nickname = 'the baby', birthday = new Date() }: { nickname?: string; birthday?: Date } = {}) => {
		const child = Child({ nickname, birthday });
		await api.addChild({ dir: datasetDir.value, child });
		await loadChildren();
	};

	const loadChildren = async () => {
		await connectDatasets();
		let results = (await api.getChildren({ dir: datasetDir.value })).value;
		if (!results.length) {
			await addChild();
			results = (await api.getChildren({ dir: datasetDir.value })).value;
		}
		children.value = results;
	};

	/* Journals */
	const loadJournals = async () => {
		const { value } = await api.getJournals({ dir: datasetDir.value });
		journalsAll.value = value;
	};

	const addJournal = async (journal: Journal) => {
		const organizer = journal.organizer ?? api.getWebId();
		if (!organizer) {
			throw new Error('addJournal - Unable to identify who is doing it');
		}
		if (!childUrl.value) {
			throw new Error('addJournal - There is no child registered');
		}

		const attendees = journal.attendees.length ?
			journal.attendees :
			dropBlanks([organizer, childUrl.value]);
		const value = createJournal({ ...journal, organizer, attendees });
		journalsAll.value = [value, ...journalsAll.value];
		await api.addJournal({ dir: datasetDir.value, value });
		await loadJournals();
	};

	const updateJournal = async (journal: Journal) => {
		const index = journalsAll.value.findIndex(({ uid }) => uid === journal.uid);
		if (index < 0) { return; }
		journalsAll.value = journalsAll.value.with(index, createJournal(journal));
		await api.addJournal({ dir: datasetDir.value, value: journal });
		await loadJournals();
	};

	const removeJournal = async (journal: Journal) => {
		const index = journalsAll.value.findIndex(({ uid }) => uid === journal.uid);
		if (index < 0) { return; }
		journalsAll.value = journalsAll.value.toSpliced(index, 1);
		await api.removeJournal({ dir: datasetDir.value, value: journal });
		await loadJournals();
	};

	const feedBaby = async ({ summary = 'Fed baby', amountMl = 25 }: { summary?: string; amountMl?: number } = {}) => {
		const categories = [FeedingCategories.BY_BABY_BOTTLE, FeedingCategories.MILK_FORMULA];
		return addJournal(JournalFeeding({ summary, amountMl, categories }));
	};

	const changeDiaper = async ({ summary = 'Changed baby' }: { summary?: string } = {}) => {
		const categories = [DiaperCategories.PEE];
		return addJournal(JournalDiaper({ summary, categories }));
	};

	const addEntry = async ({ summary }: Pick<Journal, 'summary'>) => {
		return addJournal(Journal({ summary }));
	};

	/* Storage */
	const setDatasetDir = async (path: string) => {
		datasetDir.value = path;
		await connectDatasets();
		await loadChildren();
		await loadJournals();
	};

	return {
		handleRedirect, isLoggedIn, login, logout,
		loadProfile, userName,
		child,
		datasetDir, setDatasetDir,
		childrenDatasetUrl, children, childOf, addChild, loadChildren,
		journalsDatasetUrl, journals, loadJournals, updateJournal, removeJournal,
		addEntry, changeDiaper, feedBaby,
	};
}

function rethrowAfter(fn: () => void) {
	return (err: Error) => {
		fn();
		throw err;
	};
}

function dropBlanks<T>(array: T[]): NonNullable<T>[] {
	return array.filter(element => element !== null && element !== undefined);
}
