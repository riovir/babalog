export default {
	'auth.actions.login': 'Login',
	'auth.actions.logout': 'Logout',
	'log-entry.actions.add-entry': 'Add entry',
	'log-entry.actions.change-diaper': 'Change diaper',
	'log-entry.actions.feed-baby': 'Feed baby',
	'log-entry.caption.diaper-color': 'Color',
	'log-entry.caption.diaper-summary': 'Diaper',
	'log-entry.caption.feeding-amount': 'Amount',
	'log-entry.caption.feeding-summary': 'Feeding',
	'log-entry.caption.generic-description': 'Description',
	'log-entry.caption.generic-summary': 'Summary',
	'log-entry.caption.generic-time': 'Time',
	'general.actions.apply': 'Apply',
	'general.actions.cancel': 'Cancel',
	'general.actions.clear': 'Clear',
	'general.actions.delete': 'Delete',
	'general.actions.edit': 'Edit',
	'input-date-time.caption.use-time-format': 'Use HH:MM format',
};
